package registryclient

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
	"github.com/tomnomnom/linkheader"
	"gitlab.com/zerok/container-inventory/pkg/sessionfile"
)

var ErrUnauthorized = errors.New("unauthorized")
var ErrForbidden = errors.New("forbidden")

const pageSize = 50
const defaultScheme = "https"

type Client struct {
	Host    string
	session *sessionfile.Session
	Scheme  string
}

type ClientOption func(c *Client)

func WithHost(host string) ClientOption {
	return func(c *Client) {
		c.Host = host
	}
}

type listRepositoriesResponse struct {
	Repositories []string `json:"repositories"`
}

type listTagsResponse struct {
	Tags []string `json:"tags"`
}

type Manifest struct {
	Name     string            `json:"name"`
	Tag      string            `json:"tag"`
	FSLayers []ManifestFSLayer `json:"fsLayers"`
	Digest   string
}

func (m *Manifest) BlobSum() string {
	result := strings.Builder{}
	for idx, l := range m.FSLayers {
		if idx > 0 {
			result.WriteString(" ")
		}
		result.WriteString(l.BlobSum)
	}
	return result.String()
}

type ManifestFSLayer struct {
	BlobSum string `json:"blobSum"`
}

func NewClient(options ...ClientOption) *Client {
	c := &Client{
		Scheme: defaultScheme,
	}
	for _, opt := range options {
		opt(c)
	}
	return c
}

func (c *Client) SetSession(s *sessionfile.Session) {
	c.session = s
}

func (c *Client) injectSession(req *http.Request) {
	if c.session != nil {
		req.Header.Set("Authorization", fmt.Sprintf("Basic %s", c.session.Auth.RawAuth))
	}
}

func (c *Client) buildURL(path string, args ...interface{}) string {
	p := fmt.Sprintf(path, args...)
	return fmt.Sprintf("%s://%s%s", c.Scheme, c.Host, p)
}

func (c *Client) ListRepositories(ctx context.Context) ([]string, error) {
	hc := http.Client{}
	result := make([]string, 0, 10)
	next := c.buildURL("/v2/_catalog?n=%d", pageSize)
	for {
		if next == "" {
			break
		}
		req, _ := http.NewRequest(http.MethodGet, next, nil)
		c.injectSession(req)
		resp, err := hc.Do(req.WithContext(ctx))
		if err != nil {
			return nil, errors.Wrap(err, "failed to fetch repositories")
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			switch resp.StatusCode {
			case http.StatusForbidden:
				return nil, ErrForbidden
			case http.StatusUnauthorized:
				return nil, ErrUnauthorized
			default:
				return nil, errors.Errorf("Unexpected status code: %d", resp.StatusCode)
			}
		}
		var lr listRepositoriesResponse
		if err := json.NewDecoder(resp.Body).Decode(&lr); err != nil {
			return nil, errors.Wrap(err, "failed to decode repositories response")
		}
		result = append(result, lr.Repositories...)
		next = getNext(resp)
	}
	return result, nil
}

func getNext(resp *http.Response) string {
	if links := linkheader.Parse(resp.Header.Get("Link")).FilterByRel("next"); links != nil {
		for _, link := range links {
			return link.URL
		}
	}
	return ""
}

func (c *Client) GetManifest(ctx context.Context, image string, ref string) (*Manifest, error) {
	hc := http.Client{}
	req, _ := http.NewRequest(http.MethodGet, c.buildURL("/v2/%s/manifests/%s", url.QueryEscape(image), url.QueryEscape(ref)), nil)
	c.injectSession(req)
	resp, err := hc.Do(req.WithContext(ctx))
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch repositories")
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		switch resp.StatusCode {
		case http.StatusForbidden:
			return nil, ErrForbidden
		case http.StatusUnauthorized:
			return nil, ErrUnauthorized
		default:
			return nil, errors.Errorf("Unexpected status code: %d", resp.StatusCode)
		}
	}
	var mr Manifest
	if err := json.NewDecoder(resp.Body).Decode(&mr); err != nil {
		return nil, errors.Wrap(err, "failed to decode repositories response")
	}
	mr.Digest = resp.Header.Get("Docker-Content-Digest")
	return &mr, nil
}

func (c *Client) ListTags(ctx context.Context, image string) ([]string, error) {
	hc := http.Client{}
	result := make([]string, 0, 10)
	query := url.Values{}
	query.Set("n", fmt.Sprintf("%d", pageSize))
	next := c.buildURL("/v2/%s/tags/list?%s", url.QueryEscape(image), query.Encode())
	for {
		if next == "" {
			break
		}
		req, _ := http.NewRequest(http.MethodGet, next, nil)
		c.injectSession(req)
		resp, err := hc.Do(req.WithContext(ctx))
		if err != nil {
			return nil, errors.Wrap(err, "failed to fetch repositories")
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			switch resp.StatusCode {
			case http.StatusNotFound:
				return result, nil
			case http.StatusForbidden:
				return nil, ErrForbidden
			case http.StatusUnauthorized:
				return nil, ErrUnauthorized
			default:
				return nil, errors.Errorf("Unexpected status code: %d", resp.StatusCode)
			}
		}
		var lr listTagsResponse
		if err := json.NewDecoder(resp.Body).Decode(&lr); err != nil {
			return nil, errors.Wrap(err, "failed to decode repositories response")
		}
		result = append(result, lr.Tags...)
		next = getNext(resp)
	}
	return result, nil
}
