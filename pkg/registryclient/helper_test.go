package registryclient

import (
	"fmt"
	"os/exec"
	"testing"

	"github.com/ory/dockertest"
	"github.com/stretchr/testify/require"
)

func setupRegistry(t *testing.T) *dockertest.Resource {
	t.Helper()
	pool, err := dockertest.NewPool("")
	require.NoError(t, err)
	res, err := pool.Run("registry", "2", []string{})
	require.NoError(t, err)
	return res
}

func pullImage(t *testing.T, reg *dockertest.Resource, image string) {
	t.Helper()
	require.NoError(t, exec.Command("docker", "pull", image).Run())
}

func pushImage(t *testing.T, reg *dockertest.Resource, image string) {
	t.Helper()
	require.NoError(t, exec.Command("docker", "push", image).Run())
}

func tagImage(t *testing.T, reg *dockertest.Resource, from, to string) {
	t.Helper()
	require.NoError(t, exec.Command("docker", "tag", from, to).Run())
}

func localImage(t *testing.T, reg *dockertest.Resource, image string) string {
	t.Helper()
	return fmt.Sprintf("localhost:%s/%s", reg.GetPort("5000/tcp"), image)
}

func teardownRegistry(t *testing.T, r *dockertest.Resource) {
	t.Helper()
	r.Close()
}
