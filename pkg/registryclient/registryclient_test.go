package registryclient

import (
	"context"
	"flag"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var integrating bool

func TestMain(m *testing.M) {
	flag.BoolVar(&integrating, "test.integration", false, "Run integration tests")
	flag.Parse()
	os.Exit(m.Run())
}

func TestListTags(t *testing.T) {
	if !integrating {
		t.SkipNow()
	}
	reg := setupRegistry(t)
	defer teardownRegistry(t, reg)
	ctx := context.Background()

	c := NewClient()
	c.Scheme = "http"
	c.Host = fmt.Sprintf("localhost:%s", reg.GetPort("5000/tcp"))
	time.Sleep(time.Second * 1)

	pullImage(t, reg, "hello-world:latest")
	tagImage(t, reg, "hello-world:latest", localImage(t, reg, "hello-world:latest"))
	pushImage(t, reg, localImage(t, reg, "hello-world:latest"))

	result, err := c.ListTags(ctx, "hello-world")
	require.NoError(t, err)
	require.Equal(t, result, []string{"latest"})

	// If we now request information about a repository, we should get an empty
	// list:
	result, err = c.ListTags(ctx, "not-there")
	require.NoError(t, err)
	require.Equal(t, result, []string{})
}

func TestListRepositories(t *testing.T) {
	if !integrating {
		t.SkipNow()
	}
	reg := setupRegistry(t)
	defer teardownRegistry(t, reg)
	ctx := context.Background()
	c := NewClient()
	c.Scheme = "http"
	c.Host = fmt.Sprintf("localhost:%s", reg.GetPort("5000/tcp"))
	time.Sleep(time.Second * 1)
	repos, err := c.ListRepositories(ctx)
	require.NoError(t, err)
	require.Empty(t, repos)

	// Let's now add something to the registry:
	pullImage(t, reg, "hello-world:latest")
	tagImage(t, reg, "hello-world:latest", localImage(t, reg, "hello-world:latest"))
	pushImage(t, reg, localImage(t, reg, "hello-world:latest"))

	repos, err = c.ListRepositories(ctx)
	require.NoError(t, err)
	require.Equal(t, repos, []string{"hello-world"})
}
