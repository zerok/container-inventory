package sessionfile

import (
	"flag"
	"testing"
)

const testFolderPrefix = "container-inventory-test"

var integrating bool

func TestMain(m *testing.M) {
	flag.BoolVar(&integrating, "test.integration", false, "Run integration tests")
}

func assertNoError(t *testing.T, err error) {
	t.Helper()
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
}

func assertNotNil(t *testing.T, v interface{}, msg string) {
	t.Helper()
	if v == nil {
		t.Fatal(msg)
	}
}

func assertSessionExists(t *testing.T, m *Manager, host, name string) {
	t.Helper()
	res, err := m.Exists(host, name)
	assertNoError(t, err)
	if !res {
		t.Fatalf("Session %s/%s should have existed by now.", host, name)
	}
}
