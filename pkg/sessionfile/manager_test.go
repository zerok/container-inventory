package sessionfile

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/pkg/errors"
)

func TestSessionCreation(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), testFolderPrefix)
	if err != nil {
		t.Fatalf("Failed to create directory: %s", err.Error())
	}
	defer os.RemoveAll(dir)
	m := NewManager(dir)

	t.Run("Exists for missing", func(t *testing.T) {
		result, err := m.Exists("host.com", "default")
		assertNoError(t, err)
		if result {
			t.Fatal("Expected false, got true")
		}
	})

	t.Run("Create new session", func(t *testing.T) {
		s, err := m.CreateSession("custom", "host.com", "username", "password")
		assertNoError(t, err)
		assertNotNil(t, s, "Session object is nil")
		assertSessionExists(t, m, "host.com", "custom")
	})

	t.Run("Get missing session", func(t *testing.T) {
		s, err := m.GetSession("host.com", "doesnot-exist")
		if !os.IsNotExist(errors.Cause(err)) {
			t.Fatalf("Unexpected error returned: %s. Would have expected os.ErrNotExist as cause.", err)
		}
		if s != nil {
			t.Fatalf("Would have expected nil-session, got %s instead.", s)
		}
	})
}
