package sessionfile

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

// Manager handles multiple sessions grouped by hosts.
type Manager struct {
	root string
}

// NewManager creates a new manager instance using the given path as data store.
func NewManager(path string) *Manager {
	return &Manager{root: path}
}

// GetSession retrieves a session object from the persistent data store.
func (m *Manager) GetSession(host, name string) (*Session, error) {
	path := filepath.Join(m.root, host, fmt.Sprintf("%s.json", name))
	fp, err := os.Open(path)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open session file %s", path)
	}
	defer fp.Close()
	return Parse(host, fp)
}

// Exists checks if a session of the given name exists for the provided host.
func (m *Manager) Exists(host, name string) (bool, error) {
	path := filepath.Join(m.root, host, fmt.Sprintf("%s.json", name))
	st, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	if st.IsDir() {
		return false, errors.Wrapf(err, "%s is a directory not a file", path)
	}
	return true, nil
}

// CreateSession creates a new session and persists it inside the manager.
func (m *Manager) CreateSession(name, host, username, password string) (*Session, error) {
	hostFolder := filepath.Join(m.root, host)
	path := filepath.Join(hostFolder, fmt.Sprintf("%s.json", name))
	session := NewSession(host, username, password)
	if err := os.MkdirAll(hostFolder, 0700); err != nil {
		return nil, errors.Wrapf(err, "failed to create host folder %s", hostFolder)
	}
	fp, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	defer fp.Close()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open session file for writing: %s", path)
	}
	return session, json.NewEncoder(fp).Encode(session)
}
