package sessionfile

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"

	"github.com/pkg/errors"
)

// SessionAuth represents the actual auth information of a session.
type SessionAuth struct {
	RawAuth string `json:"raw_auth"`
	Type    string `json:"type"`
}

// Session typically abstracts some kind of credential-set for a given scope
// (i.e. host).
type Session struct {
	Scope string      `json:"-"`
	Auth  SessionAuth `json:"auth"`
}

// NewSession creates a new session for the given host, username, and password.
func NewSession(host, username, password string) *Session {
	a := fmt.Sprintf("%s:%s", username, password)
	s := &Session{
		Scope: host,
		Auth: SessionAuth{
			Type:    "basic",
			RawAuth: base64.StdEncoding.EncodeToString([]byte(a)),
		},
	}
	return s
}

// Parse generates a session from the given reader.
func Parse(scope string, reader io.Reader) (*Session, error) {
	var sess Session
	if err := json.NewDecoder(reader).Decode(&sess); err != nil {
		return nil, errors.Wrap(err, "failed to decode data")
	}
	sess.Scope = scope
	return &sess, nil
}
