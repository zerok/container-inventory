BIN := "container-inventory"
all: bin/$(BIN)

bin:
	mkdir -p bin

bin/$(BIN): bin $(shell find . -name '*.go') go.mod go.sum
	cd cmd/$(BIN) && go build -o ../../bin/$(BIN)

clean:
	rm -rf bin

install: bin/$(BIN)
	cp bin/$(BIN) $(GOPATH)/bin/

test:
	go test -v ./... -cover

integration-test:
	go test -v ./... -cover -test.integration

.PHONY: clean
.PHONY: all
.PHONY: install
.PHONY: test
