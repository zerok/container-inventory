package cmd

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/zerok/container-inventory/pkg/registryclient"
	"gitlab.com/zerok/container-inventory/pkg/sessionfile"
)

var logger zerolog.Logger
var registry string
var repository string
var verbose bool
var quiet bool
var sessions *sessionfile.Manager
var longNames bool
var showVersion bool
var scheme string

// SetVersionInfo allows the main package to inject version information into the
// root command.
func SetVersionInfo(vers, com, dat string) {
	if com == "none" {
		rootCmd.Version = vers
		return
	}
	rootCmd.Version = fmt.Sprintf("%s (based on commit %s, built on %s)", vers, com, dat)
}

func showSubcommandsList(cmd *cobra.Command) {
	fmt.Println("Please use one of the following sub-commands:")
	for _, c := range cmd.Commands() {
		fmt.Printf("  %s\n", c.Use)
	}
}

var rootCmd = &cobra.Command{
	Use:           "container-inventory",
	SilenceErrors: true,
	SilenceUsage:  true,
	Run: func(cmd *cobra.Command, args []string) {
		showSubcommandsList(cmd)
	},
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if quiet {
			logger = logger.Level(zerolog.ErrorLevel)
		} else {
			if verbose {
				logger = logger.Level(zerolog.DebugLevel)
			} else {
				logger = logger.Level(zerolog.InfoLevel)
			}
		}
		u, err := user.Current()
		if err != nil {
			logger.Fatal().Err(err).Msg("Failed to determine the current user")
		}
		sessions = sessionfile.NewManager(filepath.Join(u.HomeDir, ".container-inventory", "sessions"))
	},
}

func init() {
	logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Timestamp().Logger()
	rootCmd.PersistentFlags().StringVarP(&registry, "registry", "r", "", "Registry host")
	rootCmd.PersistentFlags().StringVarP(&repository, "repository", "i", "", "Repository name")
	rootCmd.PersistentFlags().StringVarP(&sessionName, "session", "s", "", "Session name")
	rootCmd.PersistentFlags().StringVarP(&scheme, "scheme", "", "https", "Scheme (http or https)")
	rootCmd.PersistentFlags().BoolVarP(&longNames, "long", "l", false, "Display long names of tags and repositories including the registry path")
	rootCmd.PersistentFlags().BoolVar(&verbose, "verbose", false, "Verbose logging")
	rootCmd.PersistentFlags().BoolVar(&quiet, "quiet", false, "Quiet logging")
	rootCmd.InitDefaultVersionFlag()
}

func createClient() *registryclient.Client {
	rc := registryclient.NewClient(registryclient.WithHost(registry))
	rc.Scheme = scheme
	return rc
}

// MustExecute launches the root command and panics on error.
func MustExecute() {
	if err := rootCmd.Execute(); err != nil {
		logger.Fatal().Err(err).Msgf("%s failed", rootCmd.Use)
	}
}
