package cmd

import (
	"context"
	"fmt"
	"runtime"
	"sort"
	"strings"
	"sync"

	"github.com/spf13/cobra"
)

var withDigest bool
var withDetails bool

var tagsCmd = &cobra.Command{
	Use: "tags",
	Run: func(cmd *cobra.Command, args []string) {
		showSubcommandsList(cmd)
	},
}

type tagDetails struct {
	name    string
	blobsum string
	digest  string
	aliases []string
}

func removeCurrentTag(tags []string, curr string) []string {
	res := make([]string, 0, len(tags)-1)
	for _, t := range tags {
		if t != curr {
			res = append(res, t)
		}
	}
	return res
}

var listTagsCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()
		if repository == "" {
			logger.Fatal().Msg("Please specify a repository inside the registry")
		}
		c := createClient()
		if err := activateSession(c, sessionName); err != nil {
			logger.Fatal().Err(err).Msgf("Failed to load session %s for %s", sessionName, registry)
		}
		tags, err := c.ListTags(ctx, repository)
		if err != nil {
			logger.Fatal().Err(err).Msgf("Failed to retrieve tags for %s", repository)
		}
		mapping := make(map[string][]string)
		mlock := sync.RWMutex{}
		tagNames := make(chan string, 4)
		tagsWithDetails := make(chan tagDetails, 4)
		listing := make([]tagDetails, 0, len(tags))

		numWorkers := runtime.NumCPU()
		wg := sync.WaitGroup{}
		wg.Add(numWorkers + 1)
		workersDone := make(chan struct{}, 1)
		go func() {
			defer wg.Done()
			doneWorkers := 0
			for {
				select {
				case tag := <-tagsWithDetails:
					listing = append(listing, tag)
				case <-workersDone:
					doneWorkers += 1
					if doneWorkers == numWorkers {
						close(tagsWithDetails)
						return
					}
				}
			}
		}()
		for i := 0; i < numWorkers; i++ {
			go func() {
				defer wg.Done()
				for tag := range tagNames {
					t := tagDetails{name: tag}
					if withDigest || withDetails {
						logger.Debug().Msgf("Fetching details for %s", tag)
						m, err := c.GetManifest(ctx, repository, tag)
						if err != nil {
							logger.Fatal().Err(err).Msgf("Failed to retrieve manifest for %s", tag)
						}
						bs := m.BlobSum()
						mlock.Lock()
						aliases, ok := mapping[bs]
						if !ok {
							aliases = make([]string, 0, 2)
						}
						aliases = append(aliases, tag)
						mapping[bs] = aliases
						mlock.Unlock()
						t.digest = m.Digest
						t.blobsum = bs
					}
					tagsWithDetails <- t
				}
				workersDone <- struct{}{}
			}()
		}

		for _, tag := range tags {
			tagNames <- tag
		}
		close(tagNames)

		wg.Wait()

		sort.SliceStable(listing, func(i, j int) bool {
			return listing[i].name < listing[j].name
		})

		for _, tag := range listing {
			tagName := tag.name
			if longNames {
				tagName = fmt.Sprintf("%s/%s:%s", registry, repository, tag.name)
			}
			if withDetails || withDigest {
				aliases := removeCurrentTag(mapping[tag.blobsum], tag.name)
				aliasesText := strings.Builder{}
				if withDetails && len(aliases) > 0 {
					aliasesText.WriteString(" (aliases: ")
					for idx, a := range aliases {
						if idx > 0 {
							aliasesText.WriteString(", ")
						}
						aliasesText.WriteString(a)
					}
					aliasesText.WriteString(")")
				}
				digestText := strings.Builder{}
				if tag.digest != "" {
					digestText.WriteString(" (digest: ")
					digestText.WriteString(tag.digest)
					digestText.WriteString(")")
				}
				fmt.Printf("%s%s%s\n", tagName, digestText.String(), aliasesText.String())
			} else {
				fmt.Println(tagName)
			}
		}
	},
}

func init() {
	listTagsCmd.Flags().BoolVar(&withDigest, "with-digest", false, "Include the content digest (deprecated)")
	listTagsCmd.Flags().BoolVar(&withDetails, "with-details", false, "Include details about each tag")
	tagsCmd.AddCommand(listTagsCmd)
	rootCmd.AddCommand(tagsCmd)
}
