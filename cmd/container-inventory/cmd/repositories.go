package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/zerok/container-inventory/pkg/registryclient"
)

var reposCmd = &cobra.Command{
	Use:     "repositories",
	Aliases: []string{"rs"},
	Short:   `Repository commands`,
	Run: func(cmd *cobra.Command, args []string) {
		showSubcommandsList(cmd)
	},
}

var reposListCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "List all repositories",
	Run: func(cmd *cobra.Command, args []string) {
		var err error

		ctx := logger.WithContext(context.Background())
		rc := createClient()
		if err := activateSession(rc, sessionName); err != nil {
			logger.Fatal().Err(err).Msgf("Failed to load session %s for %s", sessionName, registry)
		}
		repos, err := rc.ListRepositories(ctx)
		if err != nil {
			if err == registryclient.ErrUnauthorized {
				logger.Fatal().Msg("You have to be authenticated for this operation. Please create a new session using the `session new` command.")
			}
			logger.Fatal().Err(err).Msg("Failed to retrieve repository list")
		}
		for _, r := range repos {
			if longNames {
				r = fmt.Sprintf("%s/%s", registry, r)
			}
			fmt.Println(r)
		}
	},
}

func init() {
	reposCmd.AddCommand(reposListCmd)
	rootCmd.AddCommand(reposCmd)
}
