package cmd

import (
	"github.com/bgentry/speakeasy"
	"github.com/spf13/cobra"
	"gitlab.com/zerok/container-inventory/pkg/registryclient"
)

var sessionName string
var username string
var password string

func assertRegistry() {
	if registry == "" {
		logger.Fatal().Msg("Please specify a registry (--registry NAME)")
	}
}
func assertSessionName() {
	if sessionName == "" {
		logger.Fatal().Msg("Please specify a session name (--session NAME)")
	}
}
func assertUsername() {
	if username == "" {
		logger.Fatal().Msg("Please specify a username (--username NAME)")
	}
}

func activateSession(rc *registryclient.Client, name string) error {
	if name == "" {
		if ok, _ := sessions.Exists(rc.Host, "default"); !ok {
			return nil
		}
		name = "default"
	}
	logger.Info().Msgf("Session: %s", name)
	s, err := sessions.GetSession(rc.Host, name)
	if err != nil {
		return err
	}
	rc.SetSession(s)
	return nil
}

var sessionsCmd = &cobra.Command{
	Use: "sessions",
	Run: func(cmd *cobra.Command, args []string) {
		showSubcommandsList(cmd)
	},
}

var sessionsCreateCmd = &cobra.Command{
	Use:     "create",
	Aliases: []string{"new"},
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		assertRegistry()
		assertSessionName()
		assertUsername()

		if password == "" {
			password, err = speakeasy.Ask("Password: ")
			if err != nil {
				logger.Fatal().Err(err).Msg("Failed to read the password")
			}
		}

		if _, err := sessions.CreateSession(sessionName, registry, username, password); err != nil {
			logger.Fatal().Err(err).Msgf("Failed to create session %s", sessionName)
		}
		logger.Info().Msgf("Created session %s for %s", sessionName, registry)
	},
}

func init() {
	sessionsCreateCmd.Flags().StringVar(&username, "username", "", "Username in the session")
	sessionsCreateCmd.Flags().StringVar(&password, "password", "", "Password in the session (optional)")
	sessionsCmd.AddCommand(sessionsCreateCmd)
	rootCmd.AddCommand(sessionsCmd)
}
