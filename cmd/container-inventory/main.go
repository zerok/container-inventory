package main

import "gitlab.com/zerok/container-inventory/cmd/container-inventory/cmd"

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	cmd.SetVersionInfo(version, commit, date)
	cmd.MustExecute()
}
