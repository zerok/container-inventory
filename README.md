# container-inventory

container-inventory is a little tool inspired by httpie that should make
interacting with Docker registries through the command-line easier.

My primary motiviation for creating this was that I have to work with multiple
smaller container registries at work and also in my spare-time. For those I
wanted a single, unified interface for querying, for instance, what tags were
available for a specific repository.

I was also inspired by httpie (a nicer HTTP client for the command-line) for
how it handles sessions and safe (a wrapper around the Vault CLI).

## Usage

If you have an registries that require authentication you should first create a
session. This way you don't have to enter the credentials for it every single
time:

```
# The following command will prompt you for a password:
$ container-inventory sessions create \
    --registry docker.company.com \
    --session default \
    --username username
```

You can now query docker.company.com for all the repositories on it using this
command:

```
$ container-inventory --registry docker.company.com \
    repositories list
```

If you want to know the tags of a single repository on that registry, run...

```
$ container-registry  --registry docker.company.com \
    tags list --repository projectname/imagename
```

